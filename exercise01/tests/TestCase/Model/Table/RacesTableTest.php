<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\RacesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\RacesTable Test Case
 */
class RacesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\RacesTable
     */
    protected $Races;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Races',
        'app.Records',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Races') ? [] : ['className' => RacesTable::class];
        $this->Races = $this->getTableLocator()->get('Races', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Races);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
