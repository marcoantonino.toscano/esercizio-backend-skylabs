<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\MaritalStatusesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\MaritalStatusesTable Test Case
 */
class MaritalStatusesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\MaritalStatusesTable
     */
    protected $MaritalStatuses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.MaritalStatuses',
        'app.Records',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('MaritalStatuses') ? [] : ['className' => MaritalStatusesTable::class];
        $this->MaritalStatuses = $this->getTableLocator()->get('MaritalStatuses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->MaritalStatuses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
