<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\WorkclassesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\WorkclassesTable Test Case
 */
class WorkclassesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\WorkclassesTable
     */
    protected $Workclasses;

    /**
     * Fixtures
     *
     * @var array
     */
    protected $fixtures = [
        'app.Workclasses',
        'app.Records',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Workclasses') ? [] : ['className' => WorkclassesTable::class];
        $this->Workclasses = $this->getTableLocator()->get('Workclasses', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Workclasses);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
