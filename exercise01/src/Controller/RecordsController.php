<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Records Controller
 *
 * @property \App\Model\Table\RecordsTable $Records
 * @method \App\Model\Entity\Record[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RecordsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Workclasses', 'EducationLevels', 'MaritalStatuses', 'Occupations', 'Relationships', 'Races', 'Sexes', 'Countries'],
        ];
        $records = $this->paginate($this->Records);

        $this->set(compact('records'));
    }

    /**
     * View method
     *
     * @param string|null $id Record id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $record = $this->Records->get($id, [
            'contain' => ['Workclasses', 'EducationLevels', 'MaritalStatuses', 'Occupations', 'Relationships', 'Races', 'Sexes', 'Countries'],
        ]);

        $this->set(compact('record'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $record = $this->Records->newEmptyEntity();
        if ($this->request->is('post')) {
            $record = $this->Records->patchEntity($record, $this->request->getData());
            if ($this->Records->save($record)) {
                $this->Flash->success(__('The record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The record could not be saved. Please, try again.'));
        }
        $workclasses = $this->Records->Workclasses->find('list', ['limit' => 200]);
        $educationLevels = $this->Records->EducationLevels->find('list', ['limit' => 200]);
        $maritalStatuses = $this->Records->MaritalStatuses->find('list', ['limit' => 200]);
        $occupations = $this->Records->Occupations->find('list', ['limit' => 200]);
        $relationships = $this->Records->Relationships->find('list', ['limit' => 200]);
        $races = $this->Records->Races->find('list', ['limit' => 200]);
        $sexes = $this->Records->Sexes->find('list', ['limit' => 200]);
        $countries = $this->Records->Countries->find('list', ['limit' => 200]);
        $this->set(compact('record', 'workclasses', 'educationLevels', 'maritalStatuses', 'occupations', 'relationships', 'races', 'sexes', 'countries'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Record id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $record = $this->Records->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $record = $this->Records->patchEntity($record, $this->request->getData());
            if ($this->Records->save($record)) {
                $this->Flash->success(__('The record has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The record could not be saved. Please, try again.'));
        }
        $workclasses = $this->Records->Workclasses->find('list', ['limit' => 200]);
        $educationLevels = $this->Records->EducationLevels->find('list', ['limit' => 200]);
        $maritalStatuses = $this->Records->MaritalStatuses->find('list', ['limit' => 200]);
        $occupations = $this->Records->Occupations->find('list', ['limit' => 200]);
        $relationships = $this->Records->Relationships->find('list', ['limit' => 200]);
        $races = $this->Records->Races->find('list', ['limit' => 200]);
        $sexes = $this->Records->Sexes->find('list', ['limit' => 200]);
        $countries = $this->Records->Countries->find('list', ['limit' => 200]);
        $this->set(compact('record', 'workclasses', 'educationLevels', 'maritalStatuses', 'occupations', 'relationships', 'races', 'sexes', 'countries'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Record id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $record = $this->Records->get($id);
        if ($this->Records->delete($record)) {
            $this->Flash->success(__('The record has been deleted.'));
        } else {
            $this->Flash->error(__('The record could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
    public function restapi(){
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET');
        header('Access-Control-Allow-Headers: *');
        $result = false;
        if ($this->request->is(['get'])) {
            ini_set('memory_limit', '-1');
            $params = $this->request->getQueryParams();
            // http://127.0.0.1/exercise01/esercizio-backend-skylabs/exercise01/records/restapi?aggregationType=age&aggregationValue=30
            
            if(!empty($params)){
                $result = $this->Records->find('all', [
                'contain' => ['Workclasses', 'EducationLevels', 'MaritalStatuses', 'Occupations', 'Relationships', 'Races', 'Sexes', 'Countries'],
                'limit' => 500,      // Limito al controllo di 500 records per evitare lunghi caricamenti
                ])->where([$params['aggregationType'] => $params['aggregationValue']])->toArray();

            }else{
                $result = $this->Records->find('all', [
                'contain' => ['Workclasses', 'EducationLevels', 'MaritalStatuses', 'Occupations', 'Relationships', 'Races', 'Sexes', 'Countries'],
                'limit' => 500,      // Limito al controllo di 500 records per evitare lunghi caricamenti
                ])->toArray();
                        
            }
            
        }   
        $this->set(['response' => $result]);
        $this->viewBuilder()->setOption('serialize', true);
        $this->RequestHandler->renderAs($this, 'json');
    }
    public function exportcsv(){
        $this->response = $this->response->withDownload('data.csv');
        $data = $this->Records->find('all', [
            'contain' => [], //['Workclasses', 'EducationLevels', 'MaritalStatuses', 'Occupations', 'Relationships', 'Races', 'Sexes', 'Countries'],
            'limit' => 500,
        ])->toArray();
        $_serialize = 'data';
        $this->set(compact('data'));
        $this->viewBuilder()->setClassName('CsvView.Csv')->setOption('serialize', 'data');
    }
}