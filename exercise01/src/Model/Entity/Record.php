<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Record Entity
 *
 * @property int|null $id
 * @property int|null $age
 * @property int|null $workclass_id
 * @property int|null $education_level_id
 * @property int|null $education_num
 * @property int|null $marital_status_id
 * @property int|null $occupation_id
 * @property int|null $relationship_id
 * @property int|null $race_id
 * @property int|null $sex_id
 * @property int|null $capital_gain
 * @property int|null $capital_loss
 * @property int|null $hours_week
 * @property int|null $country_id
 * @property int|null $over_50k
 *
 * @property \App\Model\Entity\Workclass $workclass
 * @property \App\Model\Entity\EducationLevel $education_level
 * @property \App\Model\Entity\MaritalStatus $marital_status
 * @property \App\Model\Entity\Occupation $occupation
 * @property \App\Model\Entity\Relationship $relationship
 * @property \App\Model\Entity\Race $race
 * @property \App\Model\Entity\Sex $sex
 * @property \App\Model\Entity\Country $country
 */
class Record extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'id' => true,
        'age' => true,
        'workclass_id' => true,
        'education_level_id' => true,
        'education_num' => true,
        'marital_status_id' => true,
        'occupation_id' => true,
        'relationship_id' => true,
        'race_id' => true,
        'sex_id' => true,
        'capital_gain' => true,
        'capital_loss' => true,
        'hours_week' => true,
        'country_id' => true,
        'over_50k' => true,
        'workclass' => true,
        'education_level' => true,
        'marital_status' => true,
        'occupation' => true,
        'relationship' => true,
        'race' => true,
        'sex' => true,
        'country' => true,
    ];
}
