<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Occupations Model
 *
 * @property \App\Model\Table\RecordsTable&\Cake\ORM\Association\HasMany $Records
 *
 * @method \App\Model\Entity\Occupation newEmptyEntity()
 * @method \App\Model\Entity\Occupation newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Occupation[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Occupation get($primaryKey, $options = [])
 * @method \App\Model\Entity\Occupation findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Occupation patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Occupation[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Occupation|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Occupation saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Occupation[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Occupation[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Occupation[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Occupation[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class OccupationsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('occupations');
        $this->setDisplayField('name');

        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->hasMany('Records', [
            'foreignKey' => 'occupation_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->allowEmptyString('id');

        $validator
            ->scalar('name')
            ->maxLength('name', 17)
            ->allowEmptyString('name');

        return $validator;
    }
}
