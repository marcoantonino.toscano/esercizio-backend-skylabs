<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Records Model
 *
 * @property \App\Model\Table\WorkclassesTable&\Cake\ORM\Association\BelongsTo $Workclasses
 * @property \App\Model\Table\EducationLevelsTable&\Cake\ORM\Association\BelongsTo $EducationLevels
 * @property \App\Model\Table\MaritalStatusesTable&\Cake\ORM\Association\BelongsTo $MaritalStatuses
 * @property \App\Model\Table\OccupationsTable&\Cake\ORM\Association\BelongsTo $Occupations
 * @property \App\Model\Table\RelationshipsTable&\Cake\ORM\Association\BelongsTo $Relationships
 * @property \App\Model\Table\RacesTable&\Cake\ORM\Association\BelongsTo $Races
 * @property \App\Model\Table\SexesTable&\Cake\ORM\Association\BelongsTo $Sexes
 * @property \App\Model\Table\CountriesTable&\Cake\ORM\Association\BelongsTo $Countries
 *
 * @method \App\Model\Entity\Record newEmptyEntity()
 * @method \App\Model\Entity\Record newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Record[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Record get($primaryKey, $options = [])
 * @method \App\Model\Entity\Record findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Record patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Record[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Record|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Record saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Record[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Record[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Record[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Record[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 */
class RecordsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('records');
        
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');
        $this->belongsTo('Workclasses', [
            'foreignKey' => 'workclass_id',
        ]);
        $this->belongsTo('EducationLevels', [
            'foreignKey' => 'education_level_id',
        ]);
        $this->belongsTo('MaritalStatuses', [
            'foreignKey' => 'marital_status_id',
        ]);
        $this->belongsTo('Occupations', [
            'foreignKey' => 'occupation_id',
        ]);
        $this->belongsTo('Relationships', [
            'foreignKey' => 'relationship_id',
        ]);
        $this->belongsTo('Races', [
            'foreignKey' => 'race_id',
        ]);
        $this->belongsTo('Sexes', [
            'foreignKey' => 'sex_id',
        ]);
        $this->belongsTo('Countries', [
            'foreignKey' => 'country_id',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id');

        $validator
            ->allowEmptyString('age');

        $validator
            ->allowEmptyString('education_num');

        $validator
            ->integer('capital_gain')
            ->allowEmptyString('capital_gain');

        $validator
            ->allowEmptyString('capital_loss');

        $validator
            ->allowEmptyString('hours_week');

        $validator
            ->allowEmptyString('over_50k');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn(['workclass_id'], 'Workclasses'), ['errorField' => 'workclass_id']);
        $rules->add($rules->existsIn(['education_level_id'], 'EducationLevels'), ['errorField' => 'education_level_id']);
        $rules->add($rules->existsIn(['marital_status_id'], 'MaritalStatuses'), ['errorField' => 'marital_status_id']);
        $rules->add($rules->existsIn(['occupation_id'], 'Occupations'), ['errorField' => 'occupation_id']);
        $rules->add($rules->existsIn(['relationship_id'], 'Relationships'), ['errorField' => 'relationship_id']);
        $rules->add($rules->existsIn(['race_id'], 'Races'), ['errorField' => 'race_id']);
        $rules->add($rules->existsIn(['sex_id'], 'Sexes'), ['errorField' => 'sex_id']);
        $rules->add($rules->existsIn(['country_id'], 'Countries'), ['errorField' => 'country_id']);

        return $rules;
    }
}
